class Shape:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def render(self):
        print('Render function')

    def scale(self, scale_factor):
        pass

    def square(self):
        pass


class Point(Shape):
    def __init__(self, x, y):
        super().__init__(x, y)


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def __contains__(self, point):
        if isinstance(point, Point):
            # if str(type(point))== "<class '__main__.Point'>":
            # return True  # circle.in_circle(point)
            return self.circle.in_circle(point)
        else:
            return False  # ('Wrong type of object')

  

    def in_circle(self, point):
        point_x = point.x
        point_y = point.y
        # if circle.x - circle.radius <= point_x <= circle.x + circle.radius \
        #      and circle.y-circle.radius<=point_y<=circle.y+circle.radius:

        if ((point_x-circle.x) ** 2 + (point_y-circle.y) ** 2) < circle.radius**2:
            return(True)
        else:
            return(False)


# point=Shape(20,20)
point = Point(10, 10)
circle = Circle(10, 10, 10)

# print(circle.in_circle())

print(circle.in_circle(point))

print(circle.__contains__(point))
print(circle.x, circle.y)
print(point.x, point.y)
