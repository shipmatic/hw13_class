class Robot():
    def __init__(self, serial='12345', weight=50, speed=10):
        self.serial = serial
        self.weight = weight
        self.speed = speed

    def charge(self, cap, charge_i):
       return('Charging time is ', cap*charge_i)




class SpotMini(Robot):
    def __init__(self, serial, weight, speed, legs):
        super().__init__(serial, weight, speed)
        self.legs = legs

    def jumps(self):
        return ('this is jump func')
        

class Atlas(Robot):
    def __init__(self, serial, weight, speed, height):
        super().__init__(serial, weight, speed)
        self.height = height
    def running(self):
        return('This is some running method')

class Handle(Robot):
    def __init__(self, serial, weight, speed, wheels):
        super().__init__(serial, weight, speed)
        self.wheels = wheels
    def handle_power(self):
        return('Lifting capacity here')
        